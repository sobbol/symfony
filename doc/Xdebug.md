# Xdebug

### Setting Xdebug from PHPStorm
1. Open PhpStorm -> Settings -> PHP -> Servers
2. Add new Server:<br>
    `Name = Docker`<br>
    `Host = 127.0.0.1`<br>
    `Port = 80`<br>
    `Project files -> Absolute path on the server set: /var/www`
   
![Basic configuration](img/xdebug.png)